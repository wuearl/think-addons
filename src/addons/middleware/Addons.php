<?php

declare(strict_types=1);

namespace think\addons\middleware;

use think\App;

class Addons
{
    protected $app;

    public function __construct(App $app)
    {
        $this->app = $app;
    }

    /**
     * 插件中间件
     * @param $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle($request, \Closure $next)
    {
        $routes = [
            '\\think\\addons\\Route@assets',
            '\\think\\addons\\Route@execute',
        ];

        $route = $request->rule()->getRoute();
        if (!in_array($route, $routes)) {
           if (is_string($route)){
               if (false !== strpos($route, '@')) {
                   $route_arr = explode('@', $route);
                   $action = array_pop($route_arr);
                   $request->setAction($action);
                   $route_arr = explode("\\", array_pop($route_arr));
                   $controller = array_pop($route_arr);
                   $request->setController($controller);
               }
           }
        }
        return $next($request);
    }
}